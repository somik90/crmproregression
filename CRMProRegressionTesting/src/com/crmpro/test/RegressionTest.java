package com.crmpro.test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.crmpro.utility.BrowserFactory;

public class RegressionTest {
	WebDriver driver;

	@BeforeMethod(description= "Testcase 1: Launch Browser")
	public void openBrowser() {
		driver = BrowserFactory.browserLaunch("chrome", "https://www.crmpro.com/index.html");
		//driver = BrowserFactory.browserLaunch("firefox","https://www.crmpro.com/index.html");
	}
	@Test(description = "Testcase 2: Check the Page Title")
	public void TestTitle() {
		BrowserFactory.matchedTitle();
	}
	
	
	@AfterMethod
	public void closeBrowser() throws InterruptedException {
		BrowserFactory.driverClose();
	}
}
