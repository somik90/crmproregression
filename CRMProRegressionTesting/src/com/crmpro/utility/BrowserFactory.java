package com.crmpro.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {

	static WebDriver driver;

	public static WebDriver browserLaunch(String browserName, String URL) {

		if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.firefox.marionette", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.get(URL);
		return driver;
	}
	public static void matchedTitle(){
		String exptitle = "CRMPRO  - CRM software for customer relationship management, sales, and support.";
		String actualtitle = driver.getTitle();
		System.out.println("The title is :" +actualtitle);
		if (exptitle.equalsIgnoreCase(actualtitle)) {
			System.out.println("Page is loaded Successfully");
		}else {
			System.out.println("Page is not loaded successfully");
		}
	}
	public static void driverClose() throws InterruptedException {
		Thread.sleep(4000);
		driver.close();
	}
}
